[![Discord invite](https://img.shields.io/badge/Chat-on_Discord-blue?style=flat-square&logo=discord&logoColor=white)](https://discord.gg/XNC86FBnQ2) [![js-semistandard-style](https://img.shields.io/badge/code%20style-semistandard-brightgreen.svg?style=flat-square)](https://github.com/standard/semistandard) [![GPL-3.0-only](https://img.shields.io/badge/license-GNU%20GPLv3-orange?style=flat-square)](https://gitlab.com/elvis-pereira/fvtt-parser/-/blob/main/LICENSE) [![version](https://img.shields.io/badge/dynamic/json?style=flat-square&color=blue&label=version&query=version&url=https%3A%2F%2Fgitlab.com%2Felvis-pereira%2Ffvtt-parser%2F-%2Fraw%2Fmain%2Fpackage.json)](https://gitlab.com/elvis-pereira/fvtt-parser/-/releases)


# Português

[English version](https://gitlab.com/elvis-pereira/fvtt-parser#english) below

Essa pequena ferramente permite gerar arquivos json a partir dos *packs* do Foundry VTT. O arquivo final pode ser um simples json com o conteúdo do *pack* ou então um arquivo de tradução que pode ser interpretado pelo [Babele](https://gitlab.com/riccisi/foundryvtt-babele).

## Instalação

Você pode instalar essa ferramenta utilizando o npm ou yarn, basta executar os passos a seguir.

**npm**
```bash
npm i @elvis-pereira/fvtt-parser
```

**yarn**
```bash
yarn add @elvis-pereira/fvtt-parser
```

## Modo de usar

Uma vez instalado você pode utilizar o comando `fvttp` para começar a gerar os arquivos.

**Exemplo**
`fvttp --path packs/classes.db --babele true`

### Opções

`--path` ou `-p`

O caminho para a pasta ou arquivo a ser convertido, essa é a única opção obrigatória.

`--ext` ou `-e`

A extensão dos arquivos que devem ser convertidos, só tem efeito quando uma pasta é passada como caminho, `.db` por padrão.

`--save` ou `-s`

Caminho onde o arquivo convertido vai ser salvo, se for omitido uma pasta chamada `output` sera criada e os arquivos serão salvos nela.

`--babele` ou `-b`

Boleano, informa se o arquivo deve ou não ser convertido em um arquivo de tradução para o Babele, `false` por padrão se for omitido.

`--mapping` ou `-m`

Arquivo contendo mapeamentos customizados a serem usados na conversão, o mapeamento padrão é o mesmo do Babele, se omitido assume a presença de um arquivo chamado `mapping.json` na raiz do projeto que sera usado caso exista.

`--use-name` ou `-n`

Boleano, determina se serão os nomes ou ID's como chaves para entradas do arquivo de tradução, `false` por padrão se for omitido (Usa ID's).

`--no-items` ou `-i`

Boleano, determina se os items contidos por um ator devem ou não ser incluídos no arquivo de tradução, se omitido os items não são incluídos.

---

# English

This small tool allows you to generate json files from Foundry VTT *packs*. The final file can be a simple json with the contents of the *pack* or a translation file that can be interpreted by [Babele](https://gitlab.com/riccisi/foundryvtt-babele).

## Installation

You can install this tool using npm or yarn, just perform the following steps.

**npm**
```bash
npm i @elvis-pereira/fvtt-parser
```

**yarn**
```bash
yarn add @elvis-pereira/fvtt-parser
```

## Usage

Once installed you can use the `fvttp` command to start generating the files.

**Example**
`fvttp --path packs/classes.db --babele true`

### Options

`--path` or `-p`

The path to the folder or file to be converted, this is the only required option.

`--ext` or `-e`

The extension of the files to be converted, only takes effect when a folder is passed as a path, `.db` by default.

`--save` or `-s`

Path where the converted file will be saved, if a folder called `output` is omitted it will be created and the files will be saved in it.

`--babele` or `-b`

Boolean, tells whether or not the file should be converted into a Babele translation file, `false` by default if omitted.

`--mapping` or `-m`

File containing custom mappings to be used in the conversion, the default mapping is the same as Babele's, if omitted it assumes the presence of a file called `mapping.json` at the root of the project that will be used if it exists.

`--use-name` or `-n`

Boolean, determines whether to use names or ID's as keys for translation file entries, `false` by default if omitted (Use ID's).

`--no-items` or `-i`

Boolean, determines whether or not items contained by an actor should be included in the translation file, if omitted items are not included.
