#!/usr/bin/env node

import { basename, extname } from 'path';
import { Command } from 'commander';
import { stat, readFileSync, readdirSync, existsSync } from 'fs';
import { toJson, createTranslation, saveFile } from './src/babele/index.js';

const program = new Command();

program
  .description('Parse a pack file into json or a babele translation json')
  .requiredOption('-p, --path <folder or file>', 'Relative path to folder or file that should be parsed')
  .option('-e, --ext <extension>', 'Extension of files that should be parsed', '.db')
  .option('-s, --save <path>', 'Path to save parsed files', 'output')
  .option('-b, --babele', 'Parse as a babele translation object', false)
  .option('-m, --mapping <path>', 'Path to file containing custom babele mappings', 'mapping.json')
  .option('-n, --use-name', 'Use the name atribute as ID', false)
  .option('-i, --no-items', 'Don\'t include actor owned items')
  .parse();

const options = program.opts();

const destinationPath = `${process.cwd()}/${options.save}`;
const mapping = existsSync(`${process.cwd()}/${options.mapping}`) ? JSON.parse(readFileSync(`${process.cwd()}/${options.mapping}`)) : {};

stat(`${process.cwd()}/${options.path}`, (error, stats) => {
  if (error) throw error;

  if (stats.isDirectory()) {
    const files = readdirSync(`${process.cwd()}/${options.path}`);
    files.filter(file => extname(file) === options.ext).forEach(file => {
      const label = file.replace(/-/g, ' ').replace(options.ext, '');
      const filePath = `${process.cwd()}/${options.path}/${file}`;
      const data = toJson(readFileSync(filePath));

      if (options.babele) {
        const translation = createTranslation(data, label, mapping, options.useName, options.items);
        saveFile(translation, destinationPath, file.replace(options.ext, '.json'));
      } else if (!options.babele) {
        saveFile(data, destinationPath, file.replace(options.ext, '.json'));
      }
    });
  } else {
    const filePath = `${process.cwd()}/${options.path}`;
    const fileName = basename(filePath, options.ext);
    const data = toJson(readFileSync(filePath));
    const label = fileName.replace(/-/g, ' ');

    if (options.babele) {
      const translation = createTranslation(data, label, mapping, options.useName, options.items);
      saveFile(translation, destinationPath, `${fileName}.json`);
    } else if (!options.babele) {
      saveFile(data, destinationPath, `${fileName}.json`);
    }
  }
});
